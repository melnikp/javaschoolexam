package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder{
    /**
     * Result pyramid
     */
    private int[][] resultPyramid;

    /**
     * Height of the resultPyramid
     */
    private int pyramidHeight;

    /**
     * Length of the resultPyramid
     */
    private int pyramidLength;

    /**
     * Input List of Integer
     */
    private List<Integer> inputList;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        inputList = inputNumbers;
        checkCorrectness();
        computePyramidDimensions();
        resultPyramid = new int[pyramidHeight][pyramidLength];
        inputList.sort(Integer::compareTo);
        fillPyramid();
        return resultPyramid;
    }

    /**
     * Check every node to null
     */
    private void checkCorrectness() {
        for (Integer integer: inputList) {
            if (integer == null) {
                throw new CannotBuildPyramidException();
            }
        }
    }

    /**
     * Compute pyramidHeight and pyramidLength
     */
    private void computePyramidDimensions() {
        int count = inputList.size();
        if (count == 0) {
            throw new CannotBuildPyramidException();
        }
        int discriminant = 1 + 8 * count;
        int root = (int)(Math.round(Math.sqrt(discriminant)));
        if (root * root == discriminant) {
            pyramidHeight = (root - 1) / 2;
            pyramidLength = pyramidHeight * 2 - 1;
        }
        else {
            throw new CannotBuildPyramidException();
        }
    }

    /**
     * Fill resultPyramid with numbers
     */
    private void fillPyramid() {
        int curIndex = 0;
        int i = 0;
        int distanceToEdge = pyramidLength / 2;
        while(i < pyramidHeight) {
            for (int j = 0; j < distanceToEdge; ++j) {
                resultPyramid[i][j] = 0;
                resultPyramid[i][pyramidLength - 1 - j] = 0;
            }
            for (int j = distanceToEdge; j < pyramidLength - distanceToEdge - 1; j += 2) {
                resultPyramid[i][j] = inputList.get(curIndex);
                resultPyramid[i][j+1] = 0;
                ++curIndex;
            }

            resultPyramid[i][pyramidLength - distanceToEdge - 1] = inputList.get(curIndex);
            ++curIndex;

            ++i;
            --distanceToEdge;
        }
    }
}
