package com.tsystems.javaschool.tasks.calculator;

import static java.lang.Math.abs;

public class NumberContainer extends Container {
    /**
     * Constant for comparing double with zero
     */
    private static final double EPS = 0.00000001;

    /**
     * Number, part of expression
     */
    private Double number;

    NumberContainer(Double number, ContainerType containerType) {
        super(containerType);
        this.number = number;
    }

    /**
     * Get number
     *
     * @return number
     */
    Double getNumber() {
        return number;
    }

    /**
     * Make some arithmetic operation with number
     *
     * @param numberContainer second argument of the operation
     * @param containerType type of the operation
     * @throws ArithmeticException if in the divide operation the absolute value of the second argument is less than EPS
     */
    void makeoperation (NumberContainer numberContainer, ContainerType containerType) throws ArithmeticException{
        switch (containerType) {
            case PLUS:
                number += numberContainer.getNumber();
                break;
            case MINUS:
                number -= numberContainer.getNumber();
                break;
            case MULTIPLY:
                number *= numberContainer.getNumber();
                break;
            case DIVIDE:
                if (abs(numberContainer.getNumber()) < EPS) {
                    throw new ArithmeticException("Zero division");
                }
                number /= numberContainer.getNumber();
        }
    }
}
