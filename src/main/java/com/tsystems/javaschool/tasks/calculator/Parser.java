package com.tsystems.javaschool.tasks.calculator;

import java.util.List;

public interface Parser {
    /**
     * Parse the input statement
     *
     * @param statement arithmetic expression
     * @return parsed arithmetic expression
     */
    List<Container> parse (String statement);
}
