package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class RPNComputer implements Computer {

    /**
     * List with parsed expression converted to postfix notation
     */
    private List<Container> postfixList;

    /**
     * Parsed arithmetic expression
     */
    List<Container> startList;

    @Override
    public String compute(List<Container> list) {
        startList = list;
        if (startList.get(0).getContainerType() != ContainerType.NUMBER)
            return null;
        try {
            makePostfix();
            return computeFromPostfix();
        }
        catch (WrongSyntaxException e) {
            return null;
        }
    }

    /**
     * Convert startList to postfix notation
     *
     * @throws WrongSyntaxException if expression is invalid
     */
    private void makePostfix () throws WrongSyntaxException {
        postfixList = new LinkedList<>();
        Stack<Container> containerStack = new Stack<>();
        for(Container container : startList) {
            if(container.getContainerType() == ContainerType.NUMBER) {
                postfixList.add(container);
            }
            else {
                parseArithmeticSymbol(container, containerStack);
            }
        }
        takeAllElementsFromStack(containerStack);
    }

    /**
     * Process non number Container
     *
     * @param container current container
     * @param containerStack stack of non number Containers
     * @throws WrongSyntaxException if expression is invalid
     */
    private void parseArithmeticSymbol(Container container, Stack<Container> containerStack) throws WrongSyntaxException{
        int priority = container.getContainerType().getPriority();
        if (containerStack.empty()) {
            containerStack.push(container);
        }
        else if (container.getContainerType() == ContainerType.R_PARENTHESIS) {
            // 2 + () 2
            Container popContainer = containerStack.pop();
            if (popContainer.getContainerType() == ContainerType.L_PARENTHESIS) {
                throw new WrongSyntaxException("Empty parentheses");
            }
            postfixList.add(popContainer);

            while (!containerStack.empty()) {
                popContainer = containerStack.pop();
                if (popContainer.getContainerType() == ContainerType.L_PARENTHESIS)
                    break;
                postfixList.add(popContainer);
            }
            if (containerStack.empty() && popContainer.getContainerType() != ContainerType.L_PARENTHESIS) {
                throw new WrongSyntaxException("No left parenthesis");
            }
        }
        else {
            int var;
            while (!containerStack.empty()
                    && (var = containerStack.peek().getContainerType().getPriority()) <= priority
                    && var > ContainerType.L_PARENTHESIS.getPriority()) {
                postfixList.add(containerStack.pop());
            }
            containerStack.push(container);
        }
    }

    /**
     * At the end of the process take elements from stack and add them to postfixList
     *
     * @param containerStack stack of non number Containers
     * @throws WrongSyntaxException if expression is invalid
     */
    private void takeAllElementsFromStack(Stack<Container> containerStack) throws WrongSyntaxException {
        while (!containerStack.isEmpty()) {
            Container container = containerStack.pop();
            if (container.getContainerType() == ContainerType.L_PARENTHESIS) {
                throw new WrongSyntaxException("Extra left parenthesis");
            }
            postfixList.add(container);
        }
    }

    /**
     * Compute the result of converted expression
     *
     * @return result String
     * @throws WrongSyntaxException if expression is invalid
     */
    private String computeFromPostfix() throws WrongSyntaxException{
        Stack<Container> containerStack = new Stack<>();
        for(Container container : postfixList) {
            if (container.getContainerType() == ContainerType.NUMBER) {
                containerStack.push(container);
            }
            else {
                try {
                    NumberContainer firstArg =  (NumberContainer)containerStack.pop();
                    NumberContainer secondArg = (NumberContainer)containerStack.peek();
                    secondArg.makeoperation(firstArg, container.getContainerType());
                }
                catch (EmptyStackException e) {
                    throw new WrongSyntaxException("Wrong order");
                }
                catch (ArithmeticException e) {
                    throw new WrongSyntaxException(e.getMessage());
                }
            }
        }
        if (containerStack.isEmpty()) {
            throw new WrongSyntaxException("Too few numbers");
        }
        NumberContainer numberContainer = (NumberContainer)containerStack.pop();
        if (!containerStack.isEmpty()) {
            throw new WrongSyntaxException("Too many numbers");
        }
        return numberContainer.getNumber().toString();
    }
}
