package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.List;

public class Calculator {

    /**
     * Scale for the result
     */
    private static final int SCALE = 4;

    /**
     * Parser of statement
     */
    private Parser parser;

    /**
     * Computer for the parsed statement
     */
    private Computer computer;

    public Calculator() {
        parser = new ParserImpl();
        computer = new RPNComputer();
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0) {
            return null;
        }
        List<Container> list = parser.parse(statement);
        if (list == null) {
            return null;
        }
        String result = computer.compute(list);
        if (result != null) {
            BigDecimal beforeRound = new BigDecimal(result);
            BigDecimal afterRound = beforeRound.setScale(SCALE, BigDecimal.ROUND_HALF_UP);
            if ((afterRound.remainder(BigDecimal.ONE)).compareTo(BigDecimal.ZERO) == 0) {
                result = String.valueOf(afterRound.intValue());
            }
            else {
                result = (new Double(afterRound.doubleValue())).toString();
            }
        }
        return result;
    }
}
