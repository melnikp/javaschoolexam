package com.tsystems.javaschool.tasks.calculator;

public enum ContainerType {
    NUMBER,
    L_PARENTHESIS,
    MULTIPLY,
    DIVIDE,
    PLUS,
    MINUS,
    R_PARENTHESIS;

    /**
     * Get the priority of exact container type
     *
     * @return priority of exact container type
     */
    public byte getPriority() {
        byte priority = 0;
        switch (this) {
            case NUMBER:
                priority = 0;
                break;
            case L_PARENTHESIS:
                priority = 1;
                break;
            case MULTIPLY:
            case DIVIDE:
                priority = 2;
                break;
            case PLUS:
            case MINUS:
                priority = 3;
                break;
            case R_PARENTHESIS:
                priority = 4;
                break;
            default:
                break;
        }
        return priority;
    }
}
