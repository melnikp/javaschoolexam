package com.tsystems.javaschool.tasks.calculator;

public class Container {
    /**
     * Type of the container
     */
    private ContainerType containerType;

    public Container(ContainerType containerType) {
        this.containerType = containerType;
    }

    /**
     * Get container type
     *
     * @return type of the container
     */
    ContainerType getContainerType() {
        return containerType;
    }
}
