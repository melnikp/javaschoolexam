package com.tsystems.javaschool.tasks.calculator;

public class WrongSyntaxException extends Exception {
    public WrongSyntaxException(String message) {
        super(message);
    }
}
