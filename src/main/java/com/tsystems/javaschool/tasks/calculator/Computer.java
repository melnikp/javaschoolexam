package com.tsystems.javaschool.tasks.calculator;

import java.util.List;

public interface Computer {
    /**
     * Compute parsed statement
     *
     * @param list parsed statement
     * @return computed result result of evaluation or null if statement is invalid
     */
    String compute (List<Container> list);
}
