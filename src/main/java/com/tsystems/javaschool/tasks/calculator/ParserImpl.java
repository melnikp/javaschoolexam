package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.List;

public class ParserImpl implements Parser{

    /**
     * List of Containers, at the end of parse it represents parsed expression
     */
    private LinkedList<Container> containerList = new LinkedList<>();

    @Override
    public List<Container> parse(String statement) {
        int i = 0;
        char var;
        ContainerType containerType;
        while (i < statement.length()) {
            var = statement.charAt(i);
            containerType = getType(var);
            if (containerType == null) {
                return null;
            }
            if (containerType == ContainerType.NUMBER) {
                try {
                    String stringNumber = readNumber(statement, i);
                    Double value = Double.parseDouble(stringNumber);
                    containerList.add(new NumberContainer(value, containerType));
                    i += stringNumber.length();
                }
                catch (WrongSyntaxException e) {
                    return null;
                }
            }
            else {
                containerList.add(new Container(containerType));
                i++;
            }
        }
        return containerList;
    }

    /**
     * Get ContainerType of var
     *
     * @param var test character
     * @return type of var
     */
    private ContainerType getType(char var) {
        if (Character.isDigit(var)) {
            return ContainerType.NUMBER;
        }
        ContainerType containerType = null;
        switch (var) {
            case '+':
                containerType = ContainerType.PLUS;
                break;
            case '-':
                containerType = ContainerType.MINUS;
                break;
            case '*':
                containerType = ContainerType.MULTIPLY;
                break;
            case '/':
                containerType = ContainerType.DIVIDE;
                break;
            case '(':
                containerType = ContainerType.L_PARENTHESIS;
                break;
            case ')':
                containerType = ContainerType.R_PARENTHESIS;
                break;
            default:
                break;
        }
        return containerType;
    }

    /**
     * Search substring with full number
     *
     * @param statement explored expression string
     * @param startInd index of where to start searching
     * @return String with full number
     * @throws WrongSyntaxException if number is invalid
     */
    private String readNumber (String statement, int startInd) throws WrongSyntaxException {
        boolean hasDot = false;
        char var;
        int i;
        for (i = startInd; i < statement.length(); ++i) {
            var = statement.charAt(i);
            if (Character.isDigit(var)) {
                continue;
            }
            else if (var == '.') {
                if (hasDot) {
                    throw new WrongSyntaxException("Too much dots");
                }
                hasDot = true;
            }
            else {
                break;
            }
        }
        if (statement.charAt(i - 1) == '.') {
            throw new WrongSyntaxException("Too much dots");
        }

        return statement.substring(startInd, i);
    }
}
