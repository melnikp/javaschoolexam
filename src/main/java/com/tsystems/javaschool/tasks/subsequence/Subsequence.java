package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        boolean hasCurrentX = true;
        ListIterator listIteratorY = y.listIterator();
        for (Object currentX : x) {
            hasCurrentX = false;
            while (listIteratorY.hasNext()) {
                if (listIteratorY.next().equals(currentX)) {
                    hasCurrentX = true;
                    break;
                }
            }
            if (!hasCurrentX) {
                return false;
            }
        }
        return true;
    }
}
